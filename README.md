# vector

Efficient implementation of Int-indexed arrays (both mutable and immutable), with a powerful loop optimisation framework . [vector](http://hackage.haskell.org/package/vector)

* [Numeric Haskell: A Vector Tutorial](http://wiki.haskell.org/Numeric_Haskell:_A_Vector_Tutorial)